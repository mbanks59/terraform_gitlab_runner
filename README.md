terraform-gitlab-runner
-----------------------

This project holds the configuration for our Gitlab Runners.


## How to add a new runner

1. Add a Gitlab Variable under Settings -> CI/CD -> Variables
  Name the variable `TF_VAR_<gitlab runner name>_registration_token`

2. Add a terraform variable in `variables.tf` to define the registration token variable name

```
# variables.tf
variable "<runner name>_registration_token" {
    type = string
}
```

3. Add a new entry in `gitlab-runners-ovh.tf`

```
# gitlab-runners-ovh.tf
module "<runner name>" {
  source                           = "git::https://gitlab.com/dreamer-labs/iac/terraform-gitlab-runner-module.git/modules/gitlab-runner/generic?ref=master"
  cloud_provider                   = "ovh"
  gitlab_runner_name               = "Test Runner"
  gitlab_runner_registration_token = var.<runner name>_registration_token
  gitlab_runner_api_token          = var.gitlab_runner_api_token
  gitlab_runner_description        = "My Test Runner"
  gitlab_runner_url                = "https://gitlab.com"
  deployment                       = "gitlab-runner"
  env_tag                          = "dev"
}
```

4. Ensure the `dreamerlabsbot` user has Maintainer access to the repository which you are attaching the Gitlab Runner to
