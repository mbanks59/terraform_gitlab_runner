module "dreamerlabs_iac_runner" {
  source                           = "git::https://gitlab.com/dreamer-labs/iac/terraform-gitlab-runner-module.git//modules/gitlab-runner/generic?ref=a0b6e55e"
  cloud_provider                   = "ovh"
  gitlab_runner_name               = "Dreamerlabs IAC Runner"
  gitlab_runner_registration_token = var.dreamerlabs_iac_runner_registration_token
  gitlab_runner_api_token          = var.gitlab_runner_api_token
  gitlab_runner_description        = "Gitlab Runner for the dreamerlabs/iac project"
  gitlab_runner_url                = "https://gitlab.com"
  deployment                       = "gitlab-runner"
  env_tag                          = "prod"
}
