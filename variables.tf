variable "gitlab_token" {
  type = string
}
variable "gitlab_runner_api_token" {
  type = string
}

variable "dreamerlabs_iac_runner_registration_token" {
  type = string
}
